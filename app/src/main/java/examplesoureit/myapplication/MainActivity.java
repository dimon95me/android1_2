package examplesoureit.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.tw1)
    TextView text;
    @BindView(R.id.action_clear_text)
    Button actionClear;
    @BindView(R.id.btn_set_text)
    Button setText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        text.setText("Yo yo yo!");


    }

    @OnClick(R.id.btn_set_text)
    void setText() {
        write("Hello world!!!");
    }

    @OnClick(R.id.action_clear_text)
    void clear() {
        write("");
    }

    private void write(String string) {
        text.setText(string);
    }
}
